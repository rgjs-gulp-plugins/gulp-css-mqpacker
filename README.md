# gulp-css-mqpacker

This plugin is a wrapper for [css-mqpacker](https://github.com/hail2u/node-css-mqpacker).

## Installation

```bash
npm install gulp-css-mqpacker
```

## Usage

```js
const gulp = require('gulp');
const gcmqp = require('gulp-css-mqpacker');

gulp.task('default', () => {
	return gulp.src('src/style.css')
		.pipe(gcmqp())
		.pipe(gulp.dest('dist'));
});
```

Based on [gulp-group-css-media-queries](https://github.com/avaly/gulp-group-css-media-queries).
