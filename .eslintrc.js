/**
 * An unopinionated baseline ESLint configuration.
 * Based on eslint-config-preact - https://github.com/preactjs/eslint-config-preact/blob/master/index.js
 */
module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  globals: {
    expect: true,
    browser: true,
    global: true
  },
  extends: [
    'eslint:recommended',
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
    ecmaFeatures: {
      modules: true,
      impliedStrict: true,
    },
  },
  ignorePatterns: [
    '**/dist/**/*.js',
  ],
  rules: {

    /**
     * General JavaScript error avoidance
     */
    'constructor-super': 2,
    'no-caller': 2,
    'no-const-assign': 2,
    'no-delete-var': 2,
    'no-dupe-class-members': 2,
    'no-dupe-keys': 2,
    'no-duplicate-imports': 2,
    'no-else-return': 1,
    'no-empty-pattern': 0,
    'no-empty': 0,
    'no-extra-parens': 0,
    'no-iterator': 2,
    'no-lonely-if': 2,
    'no-mixed-spaces-and-tabs': [1, 'smart-tabs'],
    'no-multi-str': 1,
    'no-new-wrappers': 2,
    'no-proto': 2,
    'no-redeclare': 2,
    'no-shadow-restricted-names': 2,
    'no-shadow': 0,
    'no-spaced-func': 2,
    'no-this-before-super': 2,
    'no-undef-init': 2,
    'no-unneeded-ternary': 2,
    // 'no-unused-vars': [2, { // Override, see below
    //   args: 'after-used',
    //   ignoreRestSiblings: true
    // }],
    'no-useless-call': 1,
    'no-useless-computed-key': 1,
    'no-useless-concat': 1,
    'no-useless-constructor': 1,
    'no-useless-escape': 1,
    'no-useless-rename': 1,
    'no-var': 1,
    'no-with': 2,

    /**
     * General JavaScript stylistic rules
     */
    // 'semi': 0, // Override, see below
    'strict': [2, 'never'], // assume type=module output (cli default)
    'object-curly-spacing': [0, 'always'],
    'rest-spread-spacing': 0,
    'space-before-function-paren': [0, 'always'],
    'space-in-parens': [0, 'never'],
    'object-shorthand': 1,
    'prefer-arrow-callback': 1,
    'prefer-rest-params': 1,
    'prefer-spread': 1,
    'prefer-template': 1,
    // 'quotes': [0, 'single', { // Override, see below
    //   'avoidEscape': true,
    //   'allowTemplateLiterals': true
    // }],
    'quote-props': [0, 'as-needed'],
    'radix': 1, // parseInt(x, 10)
    'unicode-bom': 2,
    'valid-jsdoc': 0,

    /**
     * Override default rules
     */
    'prettier/prettier': 0,
    'no-unused-vars': ['warn', {
      args: 'none',
      caughtErrors: 'none',
      ignoreRestSiblings: true
    }],
    'no-multiple-empty-lines': ['error', {max: 1, maxBOF: 1}],
    'indent': ['error', 2, {'SwitchCase': 1, 'MemberExpression': 0, }],
    'linebreak-style': ['error', 'unix'],
    'quotes': ['warn', 'single', {
      'avoidEscape': true,
      'allowTemplateLiterals': true
    }],
    'semi': ['warn', 'always'],
  },
};
