
const PluginError = require('plugin-error');
const through = require('through2');
const mqpacker = require('@hail2u/css-mqpacker');

module.exports = () => {
  return through.obj( function Plugin(file, enc, cb) {
    if (file.isNull()) {
      this.push(file);
      return cb();
    }

    if (file.isStream()) {
      this.emit('error', new PluginError('gulp-css-mqpacker', 'Streaming not supported'));
      return cb();
    }

    try {
      file.contents = Buffer.from(
        mqpacker.pack(file.contents.toString()).css
      );
    } catch (err) {
      this.emit('error', new PluginError('gulp-css-mqpacker', err));
    }

    this.push(file);
    cb();
  });
};
